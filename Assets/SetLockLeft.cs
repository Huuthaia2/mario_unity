using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetLockLeft : MonoBehaviour
{
    public Camera camera;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(camera.ViewportToWorldPoint(new Vector3(0f, 0f, 0f)).x, transform.position.y, 0);
    }
}
