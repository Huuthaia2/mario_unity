using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class ScreenUnit : MonoBehaviour 
{
    Camera cam;
    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        cam = Camera.main;
    }
    public float Top()
    {
        return Camera.main.ViewportToWorldPoint(new Vector3(0f, 1f, 0f)).y;
    }
    public float Bot()
    {
        return Camera.main.ViewportToWorldPoint(new Vector3(0f, 0f, 0f)).y;
    }
    public float Left()
    {
        return Camera.main.ViewportToWorldPoint(new Vector3(0f, 0f, 0f)).x;
    }
    public float Right()
    {
        return Camera.main.ViewportToWorldPoint(new Vector3(1f, 0f, 0f)).x;
    }
    public float Dis()
    {
        return Mathf.Abs(Camera.main.ViewportToWorldPoint(new Vector3(0f, 0f, 0f)).x - Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0f, 0f)).x);
    }

    public bool InScreen(Transform pos)
    {
        return Mathf.Abs(pos.position.x - Camera.main.transform.position.x) <= Dis() + .5f;
    }
}
