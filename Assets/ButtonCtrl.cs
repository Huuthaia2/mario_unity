using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonCtrl : MonoBehaviour
{
    public Mario mario;
    public string name;
    public void OnDown()
    {
        if(mario.inputFreezed) return;
        //Debug.Log("zzz OnMouseDown " + name);
        switch (name)
        {
            case "Left":
                mario.faceDirectionX = -1;
                break;
            case "Right":
                mario.faceDirectionX = 1;
                break;
            case "Jump":
                mario.jumpButtonHeld = true;
                mario.jumpButtonReleased = true;
                break;
            case "Ngoi":
                mario.isCrouching = true;
                break;
            case "Ban":

                break;

        }
    }
    public void OnOver()
    {
        if(mario.inputFreezed) return;
        //Debug.Log("zzz OnMouseOver " + name);
        switch (name)
        {
            case "Left":
                mario.faceDirectionX = -1;
                break;
            case "Right":
                mario.faceDirectionX = 1;
                break;
            case "Jump":
                // mario.jumpButtonHeld = true;
                // mario.jumpButtonReleased = true;
                break;
            case "Ngoi":
                mario.isCrouching = true;
                break;
            case "Ban":

                break;

        }
    }
    public void OnUp()
    {
        if(mario.inputFreezed) return;
        //Debug.Log("zzz OnMouseUp" + name);
        switch (name)
        {
            case "Left":
                mario.faceDirectionX = 0;
                break;
            case "Right":
                mario.faceDirectionX = 0;
                break;
            case "Jump":
                mario.jumpButtonHeld = false;
                mario.jumpButtonReleased = false;
                break;
            case "Ngoi":
                mario.isCrouching = false;
                break;
            case "Ban":

                break;

        }
    }
}
